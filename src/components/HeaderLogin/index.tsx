import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import { BackgroundDoHeader, LogoDoHeader } from "./style";
import styled from 'styled-components/native';

const Header = (props:any) => {
    return (
        <BackgroundDoHeader>
            <LogoDoHeader source={require("../../../assets/logo.png")}/>
        </BackgroundDoHeader>
    )
}


export default Header;