import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import { BotaoDeLogin, TextoBotao } from "./style";
import styled from 'styled-components/native';

const Botao = (props:any) => {
    return (
        <BotaoDeLogin>
            <TextoBotao>Continuar</TextoBotao>
        </BotaoDeLogin>
    )
}


export default Botao;