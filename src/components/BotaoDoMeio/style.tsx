import styled from "styled-components/native"

export const BotaoQuadrado = styled.TouchableOpacity`
border-radius: 25px;
height: 113px;
width: 119px;
background-color: #D9D9D9;
justify-content: center;
`
export const BotaoEstilo = styled.View`
display: flex;
flex-direction: row;
justify-content: space-around;
height: 177px;
width: 100%;
`
export const Icone = styled.Image`
height: 100%;
width: 100%;
align-self: center;

`
