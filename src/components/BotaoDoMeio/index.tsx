import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import { BotaoEstilo, BotaoQuadrado, Icone } from "./style";
import styled from 'styled-components/native';

const BotaoDoMeio = (props:any) => {
    return (
        
        <BotaoEstilo>
        
            <BotaoQuadrado>
                <Icone source={require("../../../assets/icon5.png")}/>    
            </BotaoQuadrado>
            <BotaoQuadrado>
                <Icone source={require("../../../assets/icon6.png")}/>    
            </BotaoQuadrado>
            <BotaoQuadrado>
                <Icone source={require("../../../assets/icon7.png")}/>    
            </BotaoQuadrado>
            
        </BotaoEstilo>
    )
}


export default BotaoDoMeio;