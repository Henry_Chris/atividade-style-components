import styled from "styled-components/native"

export const BotaoQuadrado = styled.TouchableOpacity`
border-radius: 25px;
height: 75px;
width: 325px;
background-color: #D9D9D9;
margin-bottom: 12px;

`
export const BotaoEstilo = styled.View`
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
width: 100%;
`