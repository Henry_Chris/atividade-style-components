import styled from "styled-components/native"

export const BackgroundDoHeader = styled.View`
display:flex;
flex-direction:row;
justify-content: space-evenly;
background-color:#FF0000;
height:140;
width:100%;
`
export const LogoDoHeader = styled.Image`
position:absolute;
top:50px;
left:17px;
height:49;
width:164;
`
export const WelcomeText = styled.Text`
position:absolute;
top:100px;
left:17px;
color:white;
font-size:20px;
`
export const BotaoDeVenda = styled.TouchableOpacity`
border-radius: 8px;
border-width: 1px;
border-color: black;
background-color:#00FF57;
height:25px;
width:76px;
padding: 5px 5px 5px 5px;
text-align:center;
position: absolute;
right:105;
bottom:15px;
`
export const BotaoDeLogout = styled.TouchableOpacity`
border-radius: 8px;
border-width: 1px;
border-color: black;
background-color:#D9D9D9;
height:25px;
width:76px;
padding: 5px 5px 5px 5px;
text-align:center;
position: absolute;
right:14px;
bottom:15px;
`
export const BotaoDeCarrinho = styled.TouchableOpacity`
border-radius: 50%;
height: 40px;
width: 40px;
position: absolute;
right:14px;
bottom:47px;
`
export const IconeDoCarrinho = styled.Image`
height:40;
width:40;
`
export const TextoBotao = styled.Text`
font-size:12px;
`