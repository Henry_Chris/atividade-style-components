import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import { BackgroundDoHeader, LogoDoHeader, WelcomeText, BotaoDeLogout, BotaoDeVenda, BotaoDeCarrinho, IconeDoCarrinho, TextoBotao } from "./style";
import styled from 'styled-components/native';

const Header = (props:any) => {
    return (
        <BackgroundDoHeader>
            <LogoDoHeader source={require("../../../assets/logo.png")}/>
            <WelcomeText>
                Bem vindo, fulano
            </WelcomeText>

            <BotaoDeVenda>
                <TextoBotao>Venda</TextoBotao>
            </BotaoDeVenda>

            <BotaoDeLogout>
                <TextoBotao>Logout</TextoBotao>
            </BotaoDeLogout>
            
            <BotaoDeCarrinho>
                <IconeDoCarrinho source={require("../../../assets/shopping-cart.png")}/>
            </BotaoDeCarrinho>
            
        </BackgroundDoHeader>
        
    )
}

export default Header;