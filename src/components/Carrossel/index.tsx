import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import { BotaoDeCarrossel, CarrosselEstilo, Icone, TextoCarrossel } from "./style";
import styled from 'styled-components/native';

const Carrossel = (props:any) => {
    return (
        
        <CarrosselEstilo>
            
            <TextoCarrossel>
                Categorias :
            </TextoCarrossel>

            <BotaoDeCarrossel>
                <Icone source={require("../../../assets/icon1.png")}/>    
            </BotaoDeCarrossel>
            <BotaoDeCarrossel>
                <Icone source={require("../../../assets/icon2.png")}/>    
            </BotaoDeCarrossel>
            <BotaoDeCarrossel>
                <Icone source={require("../../../assets/icon3.png")}/>    
            </BotaoDeCarrossel>
            <BotaoDeCarrossel>
                <Icone source={require("../../../assets/icon4.png")}/>    
            </BotaoDeCarrossel>
            
        </CarrosselEstilo>
    )
}


export default Carrossel;