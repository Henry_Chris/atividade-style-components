import styled from "styled-components/native"

export const BotaoDeCarrossel = styled.TouchableOpacity`
border-radius: 50%;
height: 80px;
width: 80px;
margin-top:58px;
`
export const CarrosselEstilo = styled.View`
display: flex;
flex-direction: row;
justify-content: space-around;

height: 189px;
width: 100%;
`
export const Icone = styled.Image`
height: 80px;
width: 80px;
`
export const TextoCarrossel = styled.Text`
font-size:12px;
left:25px;
top:28px;
position: absolute;
`