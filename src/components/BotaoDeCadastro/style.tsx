import styled from "styled-components/native"

export const BotaoDeCadastro = styled.TouchableOpacity`
flex-direction:row;
border-radius: 8px;
border-width: 1px;
border-color: black;
background-color:#D8D8D8;
justify-content:space-evenly;;
width:20%;
margin-top:2%;
margin-left:15%;
padding: 5px 5px 5px 5px;
`
export const TextoBotao = styled.Text`
font-size:12px;
`
