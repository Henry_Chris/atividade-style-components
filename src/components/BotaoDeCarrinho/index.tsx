import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import { BotaoDeCarrinhoEstilo, IconeDoCarrinho } from "./style";
import styled from 'styled-components/native';

const BotaoDeCarrinho = (props:any) => {
    return (
        <BotaoDeCarrinhoEstilo>
            <IconeDoCarrinho source={require("../../../assets/shopping-cart.png")}/>
        </BotaoDeCarrinhoEstilo>
    )
}


export default BotaoDeCarrinho;