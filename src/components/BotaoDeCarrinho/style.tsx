
import styled from "styled-components/native"

export const IconeDoCarrinho = styled.Image`
height:49;
width:164;
`
export const BotaoDeCarrinhoEstilo = styled.TouchableOpacity`
flex-direction:row;
border-radius: 8px;
border-width: 1px;
border-color: black;
background-color:#00FF57;
justify-content:space-evenly;;
width:20%;
margin-top:2%;
margin-left:15%;
padding: 5px 5px 5px 5px;
`
