import styled from "styled-components/native";

export const SearchWrapper = styled.View`
align-self:center;
border-radius: 8px;
background-color: #D9D9D9;
width:70%;
padding: 1% 1% 1% 1%;
margin-top:5px;
`

export const Search = styled.TextInput`
margin-left:5px;
color:black;
`
export const FotoDePerfil = styled.Image`
align-self:center;
margin-top:8%;
height:123;
width:99;
`
export const TextoInicial = styled.Text`
margin-left: 5px;
align-self:center;
width:70%;
font-size: 16px;
font-weight: bold;
`
export const TextoFinal = styled.Text`
margin-left: 5px;
align-self:center;
color: 878787;
width:70%;
font-size: 10px;
`