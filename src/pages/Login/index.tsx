import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import Header from "../../components/HeaderLogin";
import {SearchWrapper, Search, FotoDePerfil, TextoInicial, TextoFinal} from "./style"
import BotaoDeLogin from "../../components/BotaoDeLogin";
import BotaoDeCadastro from "../../components/BotaoDeCadastro";

const Login = ()=> {
    return (
        <View>
            <Header>

            </Header>
            <FotoDePerfil source={require("../../../assets/fotoDePerfil.png")}/>

            <TextoInicial>
                Realize o login:
            </TextoInicial>


            <SearchWrapper>
                <Search placeholder="Insira sua senha"/>
            </SearchWrapper>

            <TextoFinal>
                Esqueci minha senha
            </TextoFinal>

            <BotaoDeLogin></BotaoDeLogin>
            <BotaoDeCadastro></BotaoDeCadastro>

        </View>
    )
}

export default Login;