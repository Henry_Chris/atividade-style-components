import styled from "styled-components/native";

export const SearchWrapper = styled.View`
background-color: #D9D9D9;
width:100%;
heigth:34px;
`
export const TextoPesquisa = styled.Text`
font-size:12px;
position: absolute;
left: 10px;
top: 10px;
`
export const Search = styled.TextInput`
margin: 10px 6px 9px 78px;
border-radius: 8px;
background-color: white;
color:black;
`
export const FotoDePerfil = styled.Image`
align-self:center;
margin-top:8%;
height:123;
width:99;
`
export const TextoInicial = styled.Text`
margin-left: 5px;
align-self:center;
width:70%;
font-size: 16px;
font-weight: bold;
`
export const TextoFinal = styled.Text`
margin-left: 5px;
align-self:center;
color: 878787;
width:70%;
font-size: 10px;
`
