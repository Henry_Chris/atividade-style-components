import React from "react";
import { View, Text, Image, StyleSheet} from "react-native";
import HeaderMain from "../../components/HeaderMain";
import {SearchWrapper, Search, FotoDePerfil, TextoInicial, TextoFinal, TextoPesquisa} from "./style"
import Carrossel from "../../components/Carrossel";
import BotaoDoMeio from "../../components/BotaoDoMeio";
import BotaoDeBaixo from "../../components/BotaoDeBaixo";


const MainPage = ()=> {
    return (
        <View>
            <HeaderMain>

            </HeaderMain>

            <SearchWrapper>
                <TextoPesquisa>
                    Pesquisa : 
                </TextoPesquisa>
                <Search/>
            </SearchWrapper>

            <Carrossel>

            </Carrossel>

            <BotaoDoMeio>

            </BotaoDoMeio>

            <BotaoDeBaixo></BotaoDeBaixo>

        </View>
    )
}

export default MainPage;