import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import styled from 'styled-components';

import Login from './src/pages/Login';
import MainPage from './src/pages/MainPage';

export default function App() {
  return (
    <View>
      <MainPage/>
    </View>

  );
}

